import os
from src.helper import ts


I = lambda: None  # obj soas to add attr to it  ref https://stackoverflow.com/a/2827734/248616

I.connector_name = f'connector{ts()}test'

I.resource_name = f'resource{ts()}test'
I.resource_ip   = '10.148.0.6'
I.resource_port = '88'