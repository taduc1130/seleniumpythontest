from selenium import webdriver
from src.helper import config_wd, wait_by_xpath, wait_by_css
#
from dotenv import load_dotenv
from pathlib import Path
#
import re
SH = Path(__file__).parent
load_dotenv('../_env/.env')

from fixture.I import I



class Test:

  def test(self):
    wd = webdriver.Chrome() ; config_wd(wd)
    # def assert_dashboard_info():
    #   'dashboard @ topleft logo text'
    #   wd.get('https://www.youtube.com/')  # for some reason, we must open this url to get below code run-able  #TODO why cannot run if not refresh at this url?

    def is_valid_email(email):
      # Mẫu regex cho kiểm tra định dạng email
      pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'

      # Kiểm tra xem email có khớp với mẫu regex không
      if re.match(pattern, email):
        return True
      else:
        return False

    # Thử nghiệm với một số địa chỉ email
    email1 = "user@example.com"
    email2 = "invalid-email@"
    email3 = "another@123.ccc.co"
    email4 = "user@domain"

    print(f"{email1} is valid: {is_valid_email(email1)}")
    print(f"{email2} is valid: {is_valid_email(email2)}")
    print(f"{email3} is valid: {is_valid_email(email3)}")
    print(f"{email4} is valid: {is_valid_email(email4)}")

